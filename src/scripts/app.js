document.addEventListener("DOMContentLoaded", function () {

    const overlays = document.querySelectorAll('.overlay'),
        overlayImages = document.querySelectorAll('.people-overlay-img'),
        peopleNames = document.querySelectorAll('.people-images-text');

    for (let i = 0; i < overlays.length; ++i) {

        overlays[i].addEventListener('mouseenter', function () {
            overlayImages[i].style.opacity = "1";
            peopleNames[i].style.opacity = "1";
        });

        overlays[i].addEventListener('mouseleave', function () {
            overlayImages[i].style.opacity = "0";
            peopleNames[i].style.opacity = "0";
        });

    }
});
